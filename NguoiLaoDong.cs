﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Cache;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace QuanLyGiaoVien;

public class NguoiLaoDong
{
    public string HoTen { get; set; }
    public int NamSinh { get; set; }
    public double LuongCoBan { get; set; }

    public NguoiLaoDong() { }

    public NguoiLaoDong(string hoTen, int namSinh, double luongCoBan)
    {
        HoTen = hoTen;
        NamSinh = namSinh;
        LuongCoBan = luongCoBan;
    }

    public void NhapThongTin()
    {
        bool validName = false;
        bool validYear = false;
        bool validSalary = false;

        while (!validName)
        {
            Console.Write(" Ho ten: ");

            string nameInput = Console.ReadLine();
            var ifNumeric = int.TryParse(nameInput, out int n);
            if (string.IsNullOrEmpty(nameInput))
            {
                Console.WriteLine(" Ho ten khong the de trong");
            }
            else if (ifNumeric || HasSpecialCharOrNum(nameInput))
            {
                Console.WriteLine(" Ho ten khong hop ly");
            }
            else
            {
                HoTen = nameInput;
                validName = true;
            }
        }

        while (!validYear)
        {
            int currentYear = DateTime.Now.Year;

            Console.Write(" Nam sinh: ");

            string inputYear = Console.ReadLine();
            int birthYear = 0;
            var ifInt = int.TryParse(inputYear, out int n);

            if (!ifInt || string.IsNullOrEmpty(inputYear))
            {
                Console.WriteLine(" Nam sinh khong hop ly!");
            }
            else
            {
                birthYear = int.Parse(inputYear);
                int age = currentYear - birthYear;
                if(age < 0 || (age <= 18 || age >= 65))
                    Console.WriteLine(" Tuoi khong phu hop!");
                else
                {
                    NamSinh = int.Parse(inputYear);
                    validYear = true;
                }
            }
        }
        while (!validSalary)
        {
            Console.Write(" Nhap luong co ban: ");

            string inputSalary = Console.ReadLine();
            var ifDouble = double.TryParse(inputSalary, out double n);

            if (!ifDouble || string.IsNullOrEmpty(inputSalary))
            {
                Console.WriteLine(" Luong khong hop ly!");
            }
            else
            {
                double salary = double.Parse(inputSalary);
                if (salary < 0)
                    Console.WriteLine(" Luong khong phu hop!");
                else
                {
                    LuongCoBan = double.Parse(inputSalary);
                    validSalary = true;
                }
            }
        }
        
    }

    public double TinhLuong()
    {
        return LuongCoBan;
    }

    public void XuatThongTin()
    {
        Console.WriteLine($" Ho ten la: {HoTen}, nam sinh: {NamSinh}, luong co ban: {LuongCoBan}.");
    }

    public static bool HasSpecialCharOrNum(string input)
    {
        string pattern = @"[\W\d]";

        return Regex.IsMatch(input, pattern);
    }

}
