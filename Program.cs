﻿namespace QuanLyGiaoVien;
using System;
using System.Collections.Generic;

public class Program
{
    static void Main()
    {
        int soLuong = 0;
        List<GiaoVien> danhSachGiaoVien = new List<GiaoVien>();

        while (true)
        {
            Console.WriteLine();
            Console.WriteLine("1. Nhap so luong giao vien");
            Console.WriteLine("2. Nhap thong tin cho tung giao vien");
            Console.WriteLine("3. Hien thong tin cho tat ca giao vien");
            Console.WriteLine("4. Hien thong tin giao vien luong thap nhat");
            Console.WriteLine("5. Exit");
            Console.Write("Chon 1: ");
            string choice = Console.ReadLine();

            switch (choice)
            {
                case "1":
                    danhSachGiaoVien.Clear();
                    Console.Write("Nhap so luong giao vien: ");
                    if (int.TryParse(Console.ReadLine(), out soLuong) && soLuong > 0)
                    {
                        Console.WriteLine($"Da nhap {soLuong} giao vien.");
                    }
                    else
                    {
                        Console.WriteLine("So luong giao vien khong hop le.");
                    }
                    break;
                case "2":
                    if (soLuong == 0)
                    {
                        Console.WriteLine("Ban phai nhap so luong giao vien truoc.");
                    }
                    else
                    {
                        for (int i = 0; i < soLuong; i++)
                        {
                            GiaoVien giaoVien = new GiaoVien();
                            Console.WriteLine($"Nhap thong tin cho giao vien {i + 1}:");
                            giaoVien.NhapThongTin();
                            danhSachGiaoVien.Add(giaoVien);
                        }
                    }
                    break;

                case "3":
                    if (danhSachGiaoVien.Count == 0)
                    {
                        Console.WriteLine("Chua co thong tin giao vien.");
                    }
                    else
                    {
                        Console.WriteLine("Thong tin cho tat ca giao vien:");
                        foreach (var giaoVien in danhSachGiaoVien)
                        {
                            giaoVien.XuatThongTin();
                        }
                    }
                    break;

                case "4":
                    if (danhSachGiaoVien.Count == 0)
                    {
                        Console.WriteLine("Chua co thong tin giao vien.");
                    }
                    else
                    {
                        GiaoVien gvLuongThapNhat = danhSachGiaoVien[0];
                        foreach (var giaoVien in danhSachGiaoVien)
                        {
                            if (giaoVien.TinhLuong() < gvLuongThapNhat.TinhLuong())
                            {
                                gvLuongThapNhat = giaoVien;
                            }
                        }

                        Console.WriteLine("\nThong tin giao vien co luong thap nhat:");
                        gvLuongThapNhat.XuatThongTin();
                    }
                    break;

                case "5":
                    Console.WriteLine("Ket thuc chuong trinh.");
                    return;

                default:
                    Console.WriteLine("Lua chon khong hop le.");
                    break;
            }
        }
    }
}