﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyGiaoVien;

public class GiaoVien : NguoiLaoDong
{
    public double HeSoLuong { get; set; }

    public GiaoVien() { }

    public GiaoVien(string hoTen, int namSinh, double luongCoBan, double heSoLuong)
        : base(hoTen, namSinh, luongCoBan)
    {
        HeSoLuong = heSoLuong;
    }

    public new void NhapThongTin()
    {
        base.NhapThongTin();

        bool validCoefficient = false;
        while (!validCoefficient)
        {
            Console.Write(" Nhap he so luong: ");

            string inputCoef = Console.ReadLine();
            var ifDouble = double.TryParse(inputCoef, out double n);

            if (!ifDouble || string.IsNullOrEmpty(inputCoef))
            {
                Console.WriteLine(" He so luong khong hop ly!");
            }
            else
            {
                double salary = double.Parse(inputCoef);
                if (salary < 0)
                    Console.WriteLine(" He so luong khong phu hop!");
                else
                {
                    HeSoLuong = double.Parse(inputCoef);
                    validCoefficient = true;
                }
            }
        }
    }

    public new double TinhLuong()
    {
        return LuongCoBan * HeSoLuong * 1.25;
    }

    public new void XuatThongTin()
    {
        base.XuatThongTin();
        Console.WriteLine($" He so luong: {HeSoLuong}, Luong: {TinhLuong()}");
    }

    public void XuLy()
    {
        HeSoLuong += 0.6;
    }
}
